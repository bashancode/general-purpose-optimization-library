{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "numerical-siemens",
   "metadata": {},
   "source": [
    "# Solving continuous optimization problems.\n",
    "Traditionally, optimization problems are grouped in two distinct natural categories: those with continuous variables and those with discrete variables. When solving an instance of a continuous optimization problem, one is generally interested in finding a set of real numbers (possibly parameterizing a function). \n",
    "\n",
    "Continuous problems can be classified in two categories: unconstrained and constrained. The unconstrained continuous problems do not impose any explicit spatial constraints on the candidate solution’s validity. In practice, however, the underlying data types bound the solution representations. Contrarily, constrained problems do impose explicit spatial constraints on the solution’s validity. Following the mathematical definition, the constraints can be either hard, as they set explicit conditions for the solutions that are required to be satisfied, or soft, as they set conditions that penalize the fitness function if they are not satisfied.\n",
    "\n",
    "## Box problem type.\n",
    "We have conceptualized a module called *continuous* that contains different problem types from the continuous optimization field. In this release, the module contains one class called ``Box``: a simplistic variant of a constrained continuous problem in which the parameters can take any real number within a given range of values, the box (a.k.a. hyperrectangle), which can be regular case when the bounds are the same for every dimension or irregular when each dimension is allowed to have different bounds.\n",
    "\n",
    "### Rastrigin function.\n",
    "Rastrigin function is a non-convex, highly multimodal function used as a performance test problem for optimization algorithms. The locations of the minima are regularly distributed. Formally:\n",
    "$$ f(x) = 10d+\\sum_{i=1}^{d}[x_i^2-10cos(2 \\pi x_i)], $$\n",
    "where $d$ is the number of problem's dimensions. Visually, for $d=2$:\n",
    "\n",
    "<img src=\"https://www.sfu.ca/~ssurjano/rastr.png\" alt=\"Drawing\" style=\"width: 400px;\"/>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "japanese-monte",
   "metadata": {},
   "source": [
    "# 1. Create problem's instance."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "invisible-syntax",
   "metadata": {},
   "source": [
    "Loads the necessary classes and functions."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 38,
   "id": "thousand-dietary",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Imports PyTorch\n",
    "import torch\n",
    "# Imports problems\n",
    "from gpol.problems.continuous import Box\n",
    "from gpol.problems.utils import rastrigin_function\n",
    "# Imports metaheuristics \n",
    "from gpol.algorithms.random_search import RandomSearch\n",
    "from gpol.algorithms.local_search import HillClimbing, SimulatedAnnealing\n",
    "from gpol.algorithms.genetic_algorithm import GeneticAlgorithm\n",
    "from gpol.algorithms.differential_evolution import DifferentialEvolution\n",
    "from gpol.algorithms.swarm_intelligence import SPSO, APSO\n",
    "# Imports operators\n",
    "from gpol.operators.initializers import rnd_muniform, rnd_vuniform\n",
    "from gpol.operators.selectors import prm_dernd_selection, prm_tournament\n",
    "from gpol.operators.variators import prm_pso, prm_iball_mtn, geometric_xo, de_rand, de_exponential_xo,  de_binomial_xo, de_target_to_best"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "physical-temperature",
   "metadata": {},
   "source": [
    "Creates an instance of ``Box`` problem. The search space ($S$) of an instance of ``Box`` problem consists of the following key-value pairs:\n",
    "- ``\"constraints\"``: is a tensor that holds the lower and the upper bounds for the search space’s box. When the box is intended to be regular (i.e., all its dimensions are equally sized), it tensor holds only two values, each representing the lower and the upper bounds of each of the $D$ dimensions of the problem, respectively. When the box is intended to be irregular, the tensor is a $2$x$D$ matrix. In such case, the first and the second row represent the lower and the upper bounds for each of the $D$ dimensions of the problem, respectively; and\n",
    "-  ``\"n_dims\"``: an integer value representing the dimensionality ($d$) of $S$.\n",
    "\n",
    "When solving hard-constrained continuous problems, some authors in the scientific community bound the solutions to prevent the searching in infeasible regions. The bounding mechanism generally consists of a random re-initialization of the solution on the outlying dimension. For this reason, besides the triplet ``sspace``, ``ffunction``, and ``min_``, an instance of ``Box`` problem includes an additional instance-variable called ``bound``, which can optionally activate the solutions’ bounding by assigning it the value ``True``. When ``bound=False``, the outlying solution automatically receives the *worst* fitness score (corresponding to the largest or the smallest value allowed by the underlying data type)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 50,
   "id": "running-norman",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Defines the processing device and random state 's seed\n",
    "device, seed = \"cpu\", 0  # 'cuda' if torch.cuda.is_available() else 'cpu', 0\n",
    "# Creates the search space\n",
    "sspace = {\"constraints\": torch.tensor([-5.12, 5.12], device=device), \"n_dims\": 2}\n",
    "# Creates an instance of Box\n",
    "pi = Box(sspace=sspace, ffunction=rastrigin_function, min_=True, bound=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "assumed-slide",
   "metadata": {},
   "source": [
    "# 2. Choose and parametrize the algorithms."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "widespread-latter",
   "metadata": {},
   "source": [
    "## 2.1. Random search (RS).\n",
    "The random search (RS) can be seen as thethe first rudimentary stochastic metaheuristic for problem-solving. Its strategy, far away from being *intelligent*, consists of randomly sampling $S$ for a given number of iterations. As such, the only search-parameter of an instance of ``RandomSearch`` is the initialization function (the ``initializer``). The function ``rnd_vuniform`` is one of the numerous initializers for the single-point (SP) algorithms and it returns a uniformly distributed vector of floats."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "further-white",
   "metadata": {},
   "source": [
    "The cell in below creates a dictionary called ``pars`` which stores algorithms' parameters. Each key-value pair stores the algorithm's type and a dictionary of respective search-parameters. The first key-value pair regards the RS."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 40,
   "id": "recognized-floating",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Defines a single-point (SP) initializer\n",
    "sp_init = rnd_vuniform\n",
    "# Defines RS's parameters\n",
    "pars = {RandomSearch: {\"initializer\": sp_init}}"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "adverse-dakota",
   "metadata": {},
   "source": [
    "## 2.2. Hill climbing (HC).\n",
    "The local search (LS) algorithms can be seen among the first intelligent search strategies that improve the functioning of the RS. They rely upon the concept of neighborhood which is explored at each iteration by sampling from $S$ a limited number of neighbors of the best-so-far solution. Usually, the LS algorithms are divided in two branches. In the first branch, called hill climbing (HC), or hill descent for the minimization problems, the best-so-far solution is replaced by its neighbor when the latter is at least as good as the former."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "unnecessary-boutique",
   "metadata": {},
   "source": [
    "The cell in below adds ``HillClimbing`` to ``pars``. Note that, unlike it was for ``RandomSearch``, an instance of ``HillClimbing`` also requires the specification of a neighbor-generation function (``\"nh_function\"``) and the neighborhood's size (``\"nh_size\"``). In this example, the so-called *ball-mutation* is used, with a probability of mutating a given index of a candidate solution equal to $100$\\% (since the example is 2D), and a maximum radius of mutation equal to $0.3$.\n",
    "Note that the very same initialization function is used for both ``RandomSearch`` and ``HillClimbing``. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 51,
   "id": "balanced-merit",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Defines the size of the neighborhood \n",
    "nh_size = 100\n",
    "# Defines neighbor-generation function with the respective parameters\n",
    "p_ball_mnt, radius = 1.0, 0.3  \n",
    "nh_function = prm_iball_mtn(prob=p_ball_mnt, radius=radius)\n",
    "# Defines HC's parameters\n",
    "pars[HillClimbing] = {\"initializer\": sp_init, \"nh_function\": nh_function, \"nh_size\": nh_size}"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "responsible-persian",
   "metadata": {},
   "source": [
    "## 2.3. Simulated annealing (SA).\n",
    "The second branch, called simulated annealing (SA), extends HC by formulating a non-negative probability of replacing the best-so-far solution by its neighbor when the latter is worse. Traditionally, such a probability is small and decreases as the search advances. The strategy adopted by SA is especially useful when the search is prematurely tagnated at a locally sub-optimal point in $S$."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "revolutionary-adams",
   "metadata": {},
   "source": [
    "The cell in below adds ``SimulatedAnnealing`` to ``pars``. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 42,
   "id": "reserved-cincinnati",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Defines SA's parameters\n",
    "pars[SimulatedAnnealing] = {\"initializer\": sp_init, \"nh_function\": nh_function, \"nh_size\": nh_size, \"control\": 1.0, \"update_rate\": 0.9}"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "planned-jimmy",
   "metadata": {},
   "source": [
    "## 2.4. Genetic algorithm (GA).\n",
    "Based on the number of candidate solutions they handle at each step, the metaheuristics can be categorized into single-point (SP) and population-based (PB) approaches. \n",
    "The search procedure in the SP metaheuristics is generally guided by the information provided by a single candidate solution from $S$, usually the best-so-far solution, that is gradually evolved in a well-defined manner in hope to find the global optimum. The abovementioned HC and SA are examples of SP metaheuristics as the search is performed by exploring the neighborhood $N(i)$, where $i$ is the current best solution. Contrarily, the search procedure in PB metaheuristics is generally guided by the information shared by a set of candidate solutions and the exploitation of its collective behavior in different ways. In abstract terms, one can say that every PB metaheuristics shares, at least, the following two features: an object representing the set of simultaneously exploited candidate solutions (i.e., the population), and a procedure to *move* them across $S$.\n",
    "\n",
    "Genetic Algorithm (GAs) is a meta-heuristic introduced by J. Holland which was strongly inspired by Darwin's theory of evolution by means of natural selection. Conceptually, the algorithm starts with a random-like population of candidate solutions (called *chromosomes*). Then, by mimicking the natural selection and genetically inspired variation operators, such as the crossover and the mutation, the algorithm breeds a population of the next-generation candidate solutions (called the *offspring population*), that replaces the previous population (a.k.a. the *parent population*). This procedure is iterated until reaching some stopping criteria, like a maximum\n",
    "number of iterations (also called *generations*)."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "mechanical-burden",
   "metadata": {},
   "source": [
    "The cell in below adds ``GeneticAlgorithm`` to ``pars``. It uses a slightly different initializer, specially designed to efficiently initialize PB metaheuristics, called ``rnd_muniform``, that returns a uniformly distributed matrix of floats. Note that the mutation function is the neighbor-generation function, that was used for the aforementioned LS algorithms, and the population's size is equivalent to the neighborhood's size; this is done to foster the equivalency between LS and PB metaheuristics. Finally, the crossover is geometric, the elite is always a member of the population and parents' reproduction is enabled (``elitism=True`` and ``reproduction=True``)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 43,
   "id": "valuable-showcase",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Defines a population-based (PB) initializer\n",
    "pb_init = rnd_muniform\n",
    "# Defines GA's parameters\n",
    "pars[GeneticAlgorithm] = {\"pop_size\": nh_size, \"initializer\": pb_init, \"selector\": prm_tournament(pressure=0.05), \"mutator\": nh_function, \n",
    "                          \"crossover\": geometric_xo, \"p_m\": 0.3, \"p_c\": 0.7, \"elitism\": True, \"reproduction\": True}"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "entire-organizer",
   "metadata": {},
   "source": [
    "## 2.5. Synchronous particle swarm optimization (S-PSO).\n",
    "Particle swarm optimization (PSO) is another form of PB metaheuristic, developed by Eberhart and Kennedy in 1995. Contrarily to previously presented GA, PSO was inspired by the social behavior of living organisms, such as the birds and fish, when looking for food sources. Following PSO's nomenclature, a population is called *swarm* and a candidate solution is a *particle*.\n",
    "\n",
    "Formally, the procedure for particle's position update is defined as: \n",
    "\n",
    "$$\\vec{x}_{(p,i)} = \\vec{x}_{(p,\\ i-1)} + \\vec{v}_{(p,i)}, $$\n",
    "\n",
    "such that: \n",
    "\n",
    "$$\\vec{v}_{(p,i)} = w*\\vec{v}_{(p,\\ i-1)} + C_1\\vec{\\phi_1}(\\vec{lbest}_p - \\vec{x}_{(p,\\ i-1)}) + C_2\\vec{\\phi_2}(\\vec{gbest} - \\vec{x}_{(p,\\ i-1)}), $$ \n",
    "\n",
    "where $\\vec{lbest}_p$ and $\\vec{gbest}$ represent the local and global best, respectively, with $C_1$ and $C_2$ being two positive constants used to scale their contribution in the equation (a.k.a. acceleration coefficients). The quantities $\\vec{\\phi_1}$ and $\\vec{\\phi_2}$ are two random vectors which values follow~$\\sim$ $U(0,\\ 1)$ at each dimension. \n",
    "\n",
    "In its original definition, swarm's positions are updated taking in consideration the same version of global best particle, obtained after evaluating the whole swarm at previous iteration. That is, the global best particle is first identified and then used by all the particles in the swarm. The strength of this update method, frequently called Synchronous-PSO (S-PSO), relies in the exploitation of the information."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "empirical-episode",
   "metadata": {},
   "source": [
    "The cell in below adds ``SPSO`` to ``pars``. This time, the ``\"mutator\"`` is the function ``prm_pso`` which encapsulates PSO's update rule with parameters ``C_1``, ``C_2``, ``w_max`` and ``w_min``. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 44,
   "id": "present-architecture",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Defines SPSO's parameters\n",
    "update_rule = prm_pso(c1=1.0, c2=1.0, w_max=0.9, w_min=0.4)\n",
    "pars[SPSO] = {\"pop_size\": nh_size, \"initializer\": pb_init, \"mutator\": update_rule}"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "desperate-lawrence",
   "metadata": {},
   "source": [
    "## 2.6. Asynchronous PSO.\n",
    "Alternatively, the scientific community proposed an Asynchronous update (A-PSO), where global best particle is identified immediately after updating the position of each particle in the swarm. The strength of this update method relies in the exploration of the information."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "wicked-validity",
   "metadata": {},
   "source": [
    "The cell in below adds ``APSO`` to ``pars``. Note that the ``\"mutator\"`` function is the same as in S-PSO (``prm_apso``), suggesting that the underlying update rule remains the same."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 45,
   "id": "characteristic-graduate",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Defines APSO's parameters\n",
    "pars[APSO] = {\"pop_size\": nh_size, \"initializer\": pb_init, \"mutator\": update_rule}"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "pursuant-jewel",
   "metadata": {},
   "source": [
    "## 2.7. Differential evolution (DE).\n",
    "Differential evolution (DE) is another type of metaheuristic we have considered including in our library. Storn and Price originally designed this PB metaheuristic for solving continuous optimization problems in 1995. The algorithm shares many similar features with GA, as it involves maintaining a population of candidate solutions, which are exposed to iterative selection and variation (aka recombination). Nevertheless, DE differs substantially from GA in how the selection and the variation are performed. The parent selection is performed at random, meaning that all the chromosomes have an equal probability of being selected for mating, regardless of their fitness. The variation consists of two steps: mutation and crossover. Numerous different operators were proposed so far and many are included in this library. "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "civic-connecticut",
   "metadata": {},
   "source": [
    "The cell in below adds ``DifferentialEvolution`` to ``pars``. The function ``de_rand`` implements a generalization of the the *DE/RAND/N* mutation strategy, and is formally defined as $𝑉(𝐺+1) = 𝑉(r_1, 𝐺) + 𝐹_1[𝑉(r_2, 𝐺) − V(r_3, 𝐺)] + (...) + 𝐹_N[𝑉(r_{(N*2)}, 𝐺) − V(r_{(N*2+1)}, 𝐺)]$. In this sense, *DE/RAND/N* strategy creates the *donor* vector (i.e., $𝑉(𝐺+1)$) from adding $N$ weighted differences between $2N$ randomly selected parent vectors to another $(2N +1)^{th}$ random parent. \n",
    "\n",
    "The ``n_sols`` parameter in ``prm_dernd_selection`` function allows to adapt the desired mutation strategy for a given value of *N*: ``n_sols==3`` results in *DE/RAND/1*."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 46,
   "id": "infrared-advertiser",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Defines DE's parameters\n",
    "n_parents, m_weights = 3, torch.tensor([0.9], device=device)\n",
    "pars[DifferentialEvolution] = {\"pop_size\": nh_size, \"initializer\": pb_init, \"selector\": prm_dernd_selection(n_sols=3), \n",
    "                               \"mutator\": de_rand, \"crossover\": de_exponential_xo, \"m_weights\": m_weights, \"c_rate\": 0.5}"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "concrete-ireland",
   "metadata": {},
   "source": [
    "# 3. Executes the experiment."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "herbal-diameter",
   "metadata": {},
   "source": [
    "Note that *many* parameters and functions are shared across different algorithms in the experiment. This allows to increase the control and comparability between different algorithmic approaches when solving a given problem's instance."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 47,
   "id": "known-energy",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "<class 'gpol.algorithms.random_search.RandomSearch'>\n",
      "\t initializer <function rnd_vuniform at 0x000001B5B10A2040>\n",
      "<class 'gpol.algorithms.local_search.HillClimbing'>\n",
      "\t initializer <function rnd_vuniform at 0x000001B5B10A2040>\n",
      "\t nh_function <function prm_iball_mtn.<locals>.iball_mnt at 0x000001B5B114DE50>\n",
      "\t nh_size 100\n",
      "<class 'gpol.algorithms.local_search.SimulatedAnnealing'>\n",
      "\t initializer <function rnd_vuniform at 0x000001B5B10A2040>\n",
      "\t nh_function <function prm_iball_mtn.<locals>.iball_mnt at 0x000001B5B114DE50>\n",
      "\t nh_size 100\n",
      "\t control 1.0\n",
      "\t update_rate 0.9\n",
      "<class 'gpol.algorithms.genetic_algorithm.GeneticAlgorithm'>\n",
      "\t pop_size 100\n",
      "\t initializer <function rnd_muniform at 0x000001B5B10A20D0>\n",
      "\t selector <function prm_tournament.<locals>.tournament at 0x000001B5B116C040>\n",
      "\t mutator <function prm_iball_mtn.<locals>.iball_mnt at 0x000001B5B114DE50>\n",
      "\t crossover <function geometric_xo at 0x000001B59A3000D0>\n",
      "\t p_m 0.3\n",
      "\t p_c 0.7\n",
      "\t elitism True\n",
      "\t reproduction True\n",
      "<class 'gpol.algorithms.swarm_intelligence.SPSO'>\n",
      "\t pop_size 100\n",
      "\t initializer <function rnd_muniform at 0x000001B5B10A20D0>\n",
      "\t mutator <function prm_pso.<locals>.pso at 0x000001B5B116CCA0>\n",
      "<class 'gpol.algorithms.swarm_intelligence.APSO'>\n",
      "\t pop_size 100\n",
      "\t initializer <function rnd_muniform at 0x000001B5B10A20D0>\n",
      "\t mutator <function prm_pso.<locals>.pso at 0x000001B5B116CCA0>\n",
      "<class 'gpol.algorithms.differential_evolution.DifferentialEvolution'>\n",
      "\t pop_size 100\n",
      "\t initializer <function rnd_muniform at 0x000001B5B10A20D0>\n",
      "\t selector <function prm_dernd_selection.<locals>.dernd_selection at 0x000001B5B10E18B0>\n",
      "\t mutator <function de_rand at 0x000001B59A300310>\n",
      "\t crossover <function de_exponential_xo at 0x000001B59A300280>\n",
      "\t m_weights tensor([0.9000])\n",
      "\t c_rate 0.5\n"
     ]
    }
   ],
   "source": [
    "for isa_type, isa_pars in pars.items():\n",
    "    print(isa_type)\n",
    "    for p_name, p_val in isa_pars.items():\n",
    "        print(\"\\t\", p_name, p_val)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "banned-intellectual",
   "metadata": {},
   "source": [
    "Defines the computational resources for the experiment: the number of iterations."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 48,
   "id": "emotional-transition",
   "metadata": {},
   "outputs": [],
   "source": [
    "n_iter = 30"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "norman-giant",
   "metadata": {},
   "source": [
    "Loops the afore-defined ``pars`` dictionary containing algorithms' and the underlying parameters. Note that besides algorithm-specific parameters, the constructor of an instance of a search algorithm also receives the random state to initialize a pseudorandom number generator (called ``seed``), and the specification of the processing ``device`` (either CPU or GPU).\n",
    "\n",
    "The ``solve`` method has the same signature for all the search algorithms and, in this example, includes the following parameters: \n",
    "-  ``n_iter``: number of iterations to conduct the search;\n",
    "-  ``tol``: minimum required fitness improvement for ``n_iter_tol`` consecutive iterations to continue the search. When the fitness is not improving by at least ``tol`` for ``n_iter_tol`` consecutive iterations, the search will be automatically interrupted;\n",
    "-  ``n_iter_tol``: maximum number of iterations to not meet ``tol`` improvement;\n",
    "-  ``verbose``: verbosity's detail-level;\n",
    "-  ``log``: log-files' detail-level (if exists)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 49,
   "id": "fallen-czech",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "-------------------------------------------------\n",
      "           |           Best solution            |\n",
      "-------------------------------------------------\n",
      "Generation   Length   Fitness              Timing\n",
      "0            2        18.048                0.002\n",
      "1            2        18.048                0.000\n",
      "2            2        18.048                0.000\n",
      "3            2        18.048                0.000\n",
      "4            2        18.048                0.000\n",
      "5            2        18.048                0.000\n",
      "Algorithm: RandomSearch\n",
      "Best solution's fitness: 18.048\n",
      "Best solution: tensor([-0.0383,  2.7466])\n",
      "\n",
      "--------------------------------------------------------------------------------------\n",
      "           |           Best solution              |           Neighborhood           |\n",
      "--------------------------------------------------------------------------------------\n",
      "Generation | Length   Fitness              Timing | AVG Fitness           STD Fitness\n",
      "0          | 2        18.048                0.000 | -1                             -1\n",
      "1          | 2        8.99887               0.015 | 22.1564                   7.70516\n",
      "2          | 2        8.99887               0.014 | 19.5471                   6.15813\n",
      "3          | 2        8.99887               0.014 | 18.9989                   5.86075\n",
      "4          | 2        8.99887               0.014 | 19.5504                    6.4081\n",
      "5          | 2        8.99887               0.013 | 18.8466                   5.51379\n",
      "6          | 2        8.99887               0.014 | 19.032                    5.80097\n",
      "Algorithm: HillClimbing\n",
      "Best solution's fitness: 8.999\n",
      "Best solution: tensor([0.0105, 2.9955])\n",
      "\n",
      "--------------------------------------------------------------------------------------\n",
      "           |           Best solution              |           Neighborhood           |\n",
      "--------------------------------------------------------------------------------------\n",
      "Generation | Length   Fitness              Timing | AVG Fitness           STD Fitness\n",
      "0          | 2        18.048                0.000 | -1                             -1\n",
      "1          | 2        8.99887               0.014 | 22.1564                   7.70516\n",
      "2          | 2        9.18577               0.016 | 19.5471                   6.15813\n",
      "3          | 2        9.25931               0.013 | 18.9847                   5.77735\n",
      "4          | 2        9.68914               0.016 | 19.6677                   6.86277\n",
      "5          | 2        9.10272               0.013 | 18.8336                    5.6696\n",
      "6          | 2        9.63264               0.015 | 19.0278                   5.68596\n",
      "Algorithm: SimulatedAnnealing\n",
      "Best solution's fitness: 8.999\n",
      "Best solution: tensor([0.0105, 2.9955])\n",
      "\n",
      "--------------------------------------------------------------------------------------\n",
      "           |           Best solution              |            Population            |\n",
      "--------------------------------------------------------------------------------------\n",
      "Generation | Length   Fitness              Timing | AVG Fitness           STD Fitness\n",
      "0          | 2        2.02626               0.001 | 35.416                    16.7194\n",
      "1          | 2        2.02626               0.009 | 21.9372                   9.96694\n",
      "2          | 2        2.02626               0.011 | 18.5966                   10.8546\n",
      "3          | 2        1.84317               0.011 | 13.488                    8.96898\n",
      "4          | 2        1.33815               0.010 | 11.535                    8.90225\n",
      "5          | 2        1.09594               0.010 | 10.1467                   8.10289\n",
      "6          | 2        1.09594               0.012 | 12.4696                   10.0776\n",
      "7          | 2        1.09594               0.010 | 12.7521                    10.455\n",
      "8          | 2        1.02765               0.011 | 10.6872                   9.38585\n",
      "9          | 2        0.185802              0.011 | 6.84778                   7.34718\n",
      "10         | 2        0.185802              0.009 | 5.40177                   8.16477\n",
      "11         | 2        0.0620728             0.011 | 5.67668                   6.43977\n",
      "12         | 2        0.0620728             0.011 | 6.01237                   6.83018\n",
      "13         | 2        0.0155182             0.010 | 6.05511                   6.70472\n",
      "14         | 2        0.00979614            0.009 | 3.50426                   5.63947\n",
      "15         | 2        0.00372887            0.009 | 2.42344                   4.93015\n",
      "16         | 2        0.00248337            0.012 | 3.70181                   6.34186\n",
      "Algorithm: GeneticAlgorithm\n",
      "Best solution's fitness: 0.002\n",
      "Best solution: tensor([0.0021, 0.0029])\n",
      "\n",
      "--------------------------------------------------------------------------------------\n",
      "           |           Best solution              |            Population            |\n",
      "--------------------------------------------------------------------------------------\n",
      "Generation | Length   Fitness              Timing | AVG Fitness           STD Fitness\n",
      "0          | 2        2.02626               0.001 | 35.416                    16.7194\n",
      "1          | 2        2.02626               0.001 | 25.5627                   14.0398\n",
      "2          | 2        1.36476               0.001 | 23.1237                   11.8007\n",
      "3          | 2        1.36476               0.001 | 26.2242                   12.7656\n",
      "4          | 2        1.36476               0.002 | 26.4472                   12.5694\n",
      "5          | 2        1.26291               0.001 | 22.1961                   11.1626\n",
      "6          | 2        1.26291               0.001 | 24.7503                   11.0309\n",
      "7          | 2        1.26291               0.000 | 20.6041                   11.4914\n",
      "8          | 2        1.06031               0.000 | 22.4901                   11.0886\n",
      "9          | 2        1.06031               0.001 | 20.8897                   11.5419\n",
      "10         | 2        1.02                  0.001 | 19.2226                   11.6352\n",
      "11         | 2        1.02                  0.001 | 20.2514                   11.3246\n",
      "12         | 2        1.02                  0.000 | 19.5134                   11.2229\n",
      "13         | 2        1.02                  0.000 | 17.4403                   11.5918\n",
      "Algorithm: SPSO\n",
      "Best solution's fitness: 1.020\n",
      "Best solution: tensor([ 0.0038, -1.0055])\n",
      "\n",
      "--------------------------------------------------------------------------------------\n",
      "           |           Best solution              |            Population            |\n",
      "--------------------------------------------------------------------------------------\n",
      "Generation | Length   Fitness              Timing | AVG Fitness           STD Fitness\n",
      "0          | 2        2.02626               0.000 | 35.416                    16.7194\n",
      "1          | 2        2.02626               0.035 | 25.3089                   12.8664\n",
      "2          | 2        0.505997              0.034 | 20.6765                   11.3566\n",
      "3          | 2        0.124237              0.033 | 17.1054                   10.7874\n",
      "4          | 2        0.0318966             0.034 | 15.7056                   10.6019\n",
      "5          | 2        0.000745773           0.035 | 13.824                    11.6984\n",
      "6          | 2        9.72748e-05           0.035 | 13.887                    12.6635\n",
      "7          | 2        9.72748e-05           0.033 | 13.246                    12.7444\n",
      "8          | 2        9.72748e-05           0.033 | 11.9725                   12.2449\n",
      "Algorithm: APSO\n",
      "Best solution's fitness: 0.000\n",
      "Best solution: tensor([0.0005, 0.0005])\n",
      "\n",
      "--------------------------------------------------------------------------------------\n",
      "           |           Best solution              |            Population            |\n",
      "--------------------------------------------------------------------------------------\n",
      "Generation | Length   Fitness              Timing | AVG Fitness           STD Fitness\n",
      "0          | 2        2.02626               0.001 | 35.416                    16.7194\n",
      "1          | 2        2.02626               0.008 | 33.7703                   16.5203\n",
      "2          | 2        0.408031              0.008 | 33.0907                    16.911\n",
      "3          | 2        0.408031              0.008 | 32.6935                   16.9953\n",
      "4          | 2        0.408031              0.008 | 32.4997                   17.0653\n",
      "5          | 2        0.408031              0.008 | 31.8317                   16.3233\n",
      "6          | 2        0.408031              0.008 | 31.0984                   15.5299\n",
      "7          | 2        0.408031              0.007 | 30.7987                   15.6159\n",
      "Algorithm: DifferentialEvolution\n",
      "Best solution's fitness: 0.408\n",
      "Best solution: tensor([ 0.0310, -0.0332])\n",
      "\n"
     ]
    }
   ],
   "source": [
    "for isa_type, isa_pars in pars.items():\n",
    "    isa = isa_type(pi=pi, **isa_pars, seed=seed, device=device)\n",
    "    # n_iter*pop_size if isinstance(isa, RandomSearch) else n_iter  # equivalency for the RS\n",
    "    isa.solve(n_iter=n_iter, tol=0.1, n_iter_tol=5, verbose=2, log=0)\n",
    "    print(\"Algorithm: {}\".format(isa_type.__name__))\n",
    "    print(\"Best solution's fitness: {:.3f}\".format(isa.best_sol.fit))\n",
    "    print(\"Best solution:\", isa.best_sol.repr_, end=\"\\n\\n\")"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
